const container = document.querySelector(".conteiner");
const btn = document.querySelector(".buttonSeach");

async function getIp(url) {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(response.status);
  }
  return response.json();
}

async function getAdres() {
  const { ip } = await getIp(`https://api.ipify.org/?format=json`);
  console.log(ip);
  const geo = await getIp(
    `http://ip-api.com/json/${ip}?fields=status,continent,country,region,regionName,city,district`
  );
  return geo;
}
console.log(getAdres());
async function getFromIp() {
  const { continent, country, region, regionName, city, district } =
    await getAdres();
  container.insertAdjacentHTML(
    "afterbegin",
    `<p>Континент: ${continent}</p>
    <p>Країна: ${country}</p>
    <p>Регіон: №${region} ${regionName}</p>
    <p>Город: ${city}</p>
    <p>Район: ${district}</p>
    `
  );
}
btn.addEventListener("click", () => getFromIp());
